#include<stdio.h>
int input();
void output(int a,int b,int c,int x);
int min(int x,int y,int z);
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
void output(int a,int b,int c,int x)
{
    printf("Among %d, %d & %d %d is minimum\n",a,b,c,x);
}
int min(int x,int y,int z)
{   
    return ((x<y)&&(x<z))?x:(y<z)?y:z;
}
int main()
{
    int a,b,c,x;
    a=input();
    b=input();
    c=input();
    x=min(a,b,c);
    output(a,b,c,x);
    return 0;
}